package deber.modificadores;

public abstract class Mascota {
	protected String nombre;
	
	private String enfermedades;
	
	
	abstract public void sonido();
	public Mascota(String nombre){
		this.nombre=nombre;
	}
	
	public void setEnfermedades(String enfermedades) {
		this.enfermedades = enfermedades;
	}
	
	protected void estadoSalud(){
		if(this.enfermedades != null){
			System.out.println("salud no buena");
		}
		else{
			System.out.println("salud buena");
		}
		
	}
	
}
